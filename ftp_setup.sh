echo "Установим vsftpd"
apt install vsftpd

echo "Создадим конфигу"
echo "anonymous_enable=NO
local_enable=YES
write_enable=YES
anon_mkdir_write_enable=NO
dirmessage_enable=YES
xferlog_enable=YES
connect_from_port_20=YES
listen=YES
local_umask=002
chmod_enable=YES
file_open_mode=0777" > /etc/vsftpd.conf

echo "Создаем папку /ftpfiles"
mkdir /ftpfiles
chown ftp /ftpfiles
chmod oug+rwx /ftpfiles

systemctl enable vsftpd
systemctl restart vsftpd
