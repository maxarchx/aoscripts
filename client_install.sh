#/bin/sh
#if [ "$EUID" -ne 0 ]
#  then echo "ОШИБКА: нужны права root!"
#  exit
#fi
echo "Настройка resolv"
echo "search pgatu  pgatu.lan
nameserver 192.168.2.8" > /etc/resolv.conf

echo "Запускаем обновление"
apt update
apt upgrade

#echo "Добавим сервер времени"
#echo "server dsmain.pgatu.lan" >> /etc/ntp.conf

#echo "Запускаем службу времени"
#/etc/init.d/ntp-client start
#rc-update add ntp-client default

echo "Введите имя хоста:"
read vhostname
echo "Перезаписываем /etc/hostname"
echo "$vhostname" > /etc/hostname
echo "Перезаписываем /etc/hosts"
echo "127.0.0.1       $vhostname.pgatu.lan $vhostname localhost" > /etc/hosts

echo "Перезапустим хостнейм"
hostname "$vhostname"

echo "Установим samba"
apt install samba

echo "Установим x11vnc"
apt install x11vnc

echo "Установим sssd-tools"
apt install sssd-tools

echo "Установим krb5-user"
apt install krb5-user

echo "Установим их запуск в default"
systemctl enable smbd
systemctl enable nmbd
systemctl enable sssd

echo "Настройка samba"
echo "[global]
workgroup = PGATU
client signing = yes
client use spnego = yes
kerberos method = secrets and keytab
realm = PGATU.LAN
security = ads
log level = 3
idmap_ldb:use rfc2307 = yes" > /etc/samba/smb.conf

echo "Настройка sssd"
echo "[sssd]
services = nss, pam
config_file_version = 2
domains = PGATU.LAN

[domain/PGATU.LAN]
id_provider = ad
auth_provider = ad
chpass_provider = ad
override_homedir = /home/%u
override_shell = /bin/bash
access_provider = simple
cache_credentials = true
#cached_auth_timeout = 120

[nss]
debug_level = 1

[pam]
debug_level = 1
offline_credentials_expiration = 3
pam_verbosity = 2" > /etc/sssd/sssd.conf

chmod og-r /etc/sssd/sssd.conf 

echo "Настройка kerberos"
echo "[libdefaults]
	default_realm = PGATU.LAN
	dns_lookup_realm = true
	dns_lookup_kdc = true

[realms]
	PGATU.LAN = {
		admin_server = dsmain.pgatu.lan
	}

[logging]
	kdc = CONSOLE" > /etc/krb5.conf

echo "Правим PAM"
echo "session [default=1]                     pam_permit.so
session requisite                       pam_deny.so
session required                        pam_permit.so
session optional                        pam_umask.so
session required        pam_unix.so 
session required        pam_mkhomedir.so skel=/etc/skel/ umask=0022
session optional                        pam_sss.so 
session optional        pam_systemd.so" > /etc/pam.d/common-session

echo "Запустим samba"
systemctl restart smbd
systemctl restart nmbd

echo "Подключаемся к домену..."
kinit administrator
klist
sudo net ads join -k
systemctl restart sssd

echo "Настроим автоподмену доменных групп"
sss_override group-add linuxlocalvideo -g 27
sss_override group-add linuxlocalplugdev -g 440
sss_override group-add linuxlocalusers -g 100
sss_override group-add linuxlocalaudio -g 18
sss_override group-add linuxlocalcdrom -g 19
sss_override group-add linuxlocalfloppy -g 11
sss_override group-add linuxlocalcdrw -g 80
sss_override group-add linuxlocalscanner -g 441
sss_override group-add linuxlocalusb -g 85
sss_override group-add linuxlocalsudo -g 443
sss_override group-add linuxlocalvboxusers -g 991

echo "Запустим sssd"
systemctl restart sssd

echo "Настроим обновление DNS"
echo "Настроим обновление DNS: script"
echo "#/bin/sh
net ads dns register -Udnsregister%Dnsregister12345" > /etc/update_dns_reg.sh
chown root:root /etc/update_dns_reg.sh
chmod u+x /etc/update_dns_reg.sh
chmod og-rw /etc/update_dns_reg.sh
echo "Настроим обновление DNS: cron"
crontab -l | { cat; echo "*/10 * * * * /etc/update_dns_reg.sh > /dev/null 2>&1"; } | crontab -

echo "Установим OpenSSH"
apt install openssh-server
systemctl restart sshd 
systemctl enable sshd


echo "Исправим WPS"
rm /usr/share/mime/packages/wps-office*
update-mime-database /usr/share/mime

echo "\e[41mНЕ ЗАБЫВАЕМ ПРО qtconfig-qt4\e[49m"

#echo "Уствановим фон"
#cp ./pgatu_back.jpg /usr/pgatu_back.jpg

#echo "Обновляем конфигу темы"
#echo "[General]
#background=/usr/pgatu_back.jpg" >> /usr/share/sddm/themes/maldives/theme.conf.user
echo "Внимательно смотрим вывод, если ничего не сломалось, то все хорошо =)"
